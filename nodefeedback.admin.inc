<?php

/**
 * @file
 * Nodefeedback admin form.
 */

/**
 * Nodefeedback module default settings form.
 */
function nodefeedback_settings() {
  $form = array();
  $form['nodefeedback_form_title_default'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Node Feedback fieldset title'),
    '#default_value' => variable_get('nodefeedback_form_title_default', t('Provide feedback on this information')),
    '#size'          => 100,
    '#maxlength'     => 200,
    '#required'      => TRUE,
  );
  $form['nodefeedback_questions_array_default'] = array(
    '#type'          => 'textarea',
    '#title'         => t('Node Feedback questions array config - BE VERY CAREFUL WITH IT AS IT USES PHP eval() FUNCTION TO RUN IT'),
    '#description'   => t('Enter array of questions to ask on the feedback form. Keys description: tag - tag to put into votingapi or nodefeedback database tables; title - the question itself; type - currently only radios and textarea types are supported; description - a description note to justify/clarify the question; required - does this question requires an answer? TRUE or FALSE value; storage - use "votingapi" for the radios type and "nodefeedback" for the textareas; answer_options - array of answer options for the radios type.'),
    '#default_value' => variable_get('nodefeedback_questions_array_default', nodefeedback_questions_config_default()),
    '#cols'          => 80,
    '#rows'          => 15,
    '#required'      => FALSE,
    '#access'        => user_access('use PHP input for nodefeedback options'),
  );
  $form['nodefeedback_button_text_default'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Text on the Submit button'),
    '#default_value' => variable_get('nodefeedback_button_text_default', t('Send')),
    '#size'          => 20,
    '#maxlength'     => 20,
    '#required'      => TRUE,
  );
  $form['nodefeedback_confirmation_message_default'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Message to display after feedback submission'),
    '#default_value' => variable_get('nodefeedback_confirmation_message_default', t('Thank you! Your feedback is used to help us improve our content.')),
    '#size'          => 100,
    '#maxlength'     => 200,
    '#required'      => FALSE,
  );

  return system_settings_form($form);
}