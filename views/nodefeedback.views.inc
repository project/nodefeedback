<?php

/**
 * @file
 * Provide views data for nodefeedback.module.
 */

/**
 * Implementation of hook_views_data().
 */
function nodefeedback_views_data() {
  // Basic table information.

  // Define the base group of this table. Fields that don't
  // have a group defined will go into this group by default.
  $data['nodefeedback']['table']['group']  = t('Node feedback');
  // Define nodefeedback as a base table to create a view.
  $data['nodefeedback']['table']['base'] = array(
    'field' => 'nodefeedback_id',
    'title' => t('Node feedback'),
    'help' => t('Node feedback is the text users leave on the nodes feedback form.'),
    'weight' => 0,
  );
  // Define join with the node table.
  $data['nodefeedback']['table']['join']['node'] = array(
    'left_field' => 'nid',
    'field' => 'content_id',
  );

  // ----------------------------------------------------------------
  // Fields

  //content_id
  $data['nodefeedback']['content_id'] = array(
    'title' => t('Node ID'),
    'help' => t('Relate a node feedback to the node.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'node',
      'base field' => 'nid',
      'label' => t('node'),
    ),
  );
  
  // value
  $data['nodefeedback']['value'] = array(
    'title' => t('Feedback'),
    'help' => t('The feedback text.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );

  // tag
  $data['nodefeedback']['tag'] = array(
    'title' => t('Tag'),
    'help' => t('An optional tag to group multi-criteria feedbacks.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  // uid
  $data['nodefeedback']['uid'] = array(
    'title' => t('User'),
    'help' => t('The user who left the feedback.'),
    'relationship' => array(
      'handler' => 'views_handler_relationship',
      'base' => 'users',
      'base field' => 'uid',
      'label' => t('user'),
    ),
  );

  // timestamp
  $data['nodefeedback']['timestamp'] = array(
    'title' => t('Timestamp'),
    'help' => t('The time the vote was cast.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
  );
  
  // vote_source
  $data['nodefeedback']['vote_source'] = array(
    'title' => t('IP Address'),
    'help' => t('The IP address of the user who cast the vote.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  return $data;
}

/**
 * Implementation of hook_date_api_tables().
 */
function nodefeedback_date_api_tables() {
  return array('nodefeedback');
}

/**
 * Implementation of hook_date_api_fields().
 */
function nodefeedback_date_api_fields($field) {
  if ($field == 'nodefeedback.timestamp') {
    return array(
      // The type of date: DATE_UNIX, DATE_ISO, DATE_DATETIME.
      'sql_type' => DATE_UNIX,
      // Timezone handling options: 'none', 'site', 'date', 'utc'.
      'tz_handling' => 'utc',
      // Granularity of this date field's db data.
      'granularity' => array('year', 'month', 'day', 'hour', 'minute', 'second'),
    );
  }
}